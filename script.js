document.addEventListener('DOMContentLoaded', function () {
    // Set the duration for each slide in milliseconds (e.g., 10 seconds = 10000 milliseconds)
    const slideDuration = 10000;

    const slidesContainer = document.querySelector('.slides');
    const slides = document.querySelectorAll('.slide');
    let currentSlideIndex = 0;

    // Set an interval to move to the next slide after the specified duration
    setInterval(nextSlide, slideDuration);

    function nextSlide() {
       currentSlideIndex = (currentSlideIndex + 1) % slides.length;

        // Animate the transition to the next slide
        slidesContainer.style.transition = 'transform 1s ease-in-out'; /* Adjust the transition duration */
        slidesContainer.style.transform = `translateX(-${currentSlideIndex * 100}vw)`;

        // Remove the transition after the animation
        setTimeout(() => {
            slidesContainer.style.transition = 'none';
        }, 1000); /* Adjust the timeout based on the transition duration */
    }
});
